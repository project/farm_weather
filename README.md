# farmOS Weather

Provides a quick form for tracking precipitation observations in
[farmOS](https://farmOS.org).

## Maintainers

Current maintainers:
- [Michael Stenta](https://github.com/mstenta)

This project has been sponsored by:
- [Farmier](http://farmier.com)
- [Globetrotter Foundation](http://globetrotterfoundation.org)
